const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const AWS = require('aws-sdk');
const router = require('./router');

const PORT_NUMBER = 3001;

app.use(cors());
// Fire up healthcheck endpoint
app.use(bodyParser.json());

router.get('/health', (req, res) => {
    res.sendStatus(200);
});

app.use('/', router);

app.use(function (req, res) {
    res.status(404);
    res.send({ error: "not Found - error 404" });
});

app.listen(PORT_NUMBER, () => {
    console.info(`Server listening @ http://localhost:${PORT_NUMBER}`);
});