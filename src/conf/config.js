// Check for mandatory environment variables
const required = [
  'AUTH_URL'
];

required.forEach(param => {
    if (!process.env[param]) {
        throw new Error(`Environment parameter ${param} is missing`);
    }
});

const config = {
  env: process.env['NODE_ENV'],
  auth_url: process.env['AUTH_URL'],
  codes: {
    serverError: 500,
    badRequest: 400,
    success: 200,
  },
};

module.exports = config;