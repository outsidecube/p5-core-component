const { codes } = require('../conf/config');
const authRepository = require('../repositories/auth.repository');

class usuariosController {
  static async getUsuario(req, res) {
    let token = req.headers['authorization'];

    if (!token)       
      res.status(400).send({
        msg: 'missing token'
      });

    token = token.replace('Bearer ', '');

    try {
      const result = await authRepository.verifyToken(token);
      console.log('result: ', result);
      return res.status(200).send(result.data);
    } catch (error) {
      res.status(codes.serverError).send({
        channel,
        msg: 'error'
      });
    }
  }
}

module.exports = usuariosController;