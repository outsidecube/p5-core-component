const { auth_url } = require('../conf/config');
const axios = require('axios');

const client = axios.create({});

class authRepository {

  static async verifyToken(token) {
    const data = { token };

    try {
      return client.post(auth_url, data);
    } catch (error) {
      console.log('error: ', error);
    }
  }
}
module.exports = authRepository;
