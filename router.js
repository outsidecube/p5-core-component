const express = require('express');
const UsuariosController = require('./src/controllers/usuarios.controller');
const router  = express.Router();

const version = 'v1';

router.get(`/${version}/usuarios`, UsuariosController.getUsuario);

module.exports = router;
